Useful links: 

YouTube Analytics API [README](https://gitlab.com/szczepanik_carl/ytb_analytics_api_carl/-/blob/main/README.md?ref_type=heads) - has some useful information relating to this project (setting up OAuth)

Docs from YouTube:

* [Report types](https://developers.google.com/youtube/reporting/v1/reports/content_owner_reports) for content owners

* Python [code samples](https://developers.google.com/youtube/reporting/v1/code_samples/python)

contact: frantisek.szczepanik@hicarl.cz
