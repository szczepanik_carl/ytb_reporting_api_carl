import argparse
import os

import google.oauth2.credentials
import google_auth_oauthlib.flow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from google_auth_oauthlib.flow import InstalledAppFlow
from io import FileIO
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
import datetime
import json



"""This code automatically downloads the latest reports for 6 specified report types,
 and stores them in the folder reports/."""


CLIENT_SECRETS_FILE = 'auth2.json'
SCOPES = ['https://www.googleapis.com/auth/yt-analytics-monetary.readonly']
API_SERVICE_NAME = 'youtubereporting'
API_VERSION = 'v1'

# Authorize the request and store authorization credentials.
def get_service(channel_id):
  credentials = None
  # Check if credentials file exists
  CREDENTIALS_FILE = channel_id + ".json"
  if os.path.exists(CREDENTIALS_FILE):
    credentials = Credentials.from_authorized_user_file(CREDENTIALS_FILE, SCOPES)
  else:
    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
    credentials = flow.run_console()

  # If no valid credentials, use InstalledAppFlow to authenticate
  if not credentials or not credentials.valid:
    if credentials and credentials.expired and credentials.refresh_token:
      print(credentials.refresh_token)
      credentials.refresh(Request())


  # Save the credentials for the next run
  with open(CREDENTIALS_FILE, 'w') as json_file:
    json_file.write(credentials.to_json())

  return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)



# Call the YouTube Reporting API's reports.list method to get the URL of the latest report of a given job
def retrieve_latest_report_URL(job_id, youtube_reporting, content_owner_id):

  latest_reports = youtube_reporting.jobs().reports().list(
       onBehalfOfContentOwner = content_owner_id,
       jobId = job_id,
       startTimeAtOrAfter=(datetime.datetime.utcnow() - datetime.timedelta(days=3)).isoformat() + "Z" # list only the report for the latest day
  ).execute()

  if 'reports' in latest_reports and latest_reports['reports']:
    report = latest_reports['reports'][0]
    return report['downloadUrl']

# Call the YouTube Reporting API's media.download method to download the report.
def download_report(youtube_reporting, report_url, local_file):
  request = youtube_reporting.media().download(
    resourceName=' '
  )
  request.uri = report_url
  fh = FileIO(local_file, mode='wb')
  # Stream/download the report in a single request.
  downloader = MediaIoBaseDownload(fh, request, chunksize=-1)

  done = False
  while done is False:
    status, done = downloader.next_chunk()
    if status:
      print('Download %d%%.' % int(status.progress() * 100))
  print('Download Complete!')


# retrieve the latest URL for a given report, download it, and store in a target file as a csv
def process_report(job_id, youtube_reporting, content_owner_id, filename):
  url = retrieve_latest_report_URL(job_id,youtube_reporting,content_owner_id)
  download_report(youtube_reporting, url, filename)



if __name__ == '__main__':
  with open("job_records.json", "r") as f:
    data_for_report_retrieval = json.load(f)
  
  content_owner_id = data_for_report_retrieval["content_owner_id"]
  youtube_reporting = get_service(content_owner_id)

  job_records = data_for_report_retrieval["job_records"]
  for job_record in job_records:
    job_id = job_record["id"]
    target_file = job_record["targetFile"]

    process_report(job_id, youtube_reporting, content_owner_id, target_file)

  print("ok")
