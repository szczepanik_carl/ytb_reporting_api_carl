import os
import google.oauth2.credentials
import google_auth_oauthlib.flow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from google_auth_oauthlib.flow import InstalledAppFlow
from io import FileIO
from io import BytesIO
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
import datetime
import json
import pandas as pd
import pandas_gbq

CLIENT_SECRETS_FILE = 'auth.json'
SCOPES = ['https://www.googleapis.com/auth/yt-analytics-monetary.readonly']
API_SERVICE_NAME = 'youtubereporting'
API_VERSION = 'v1'

# Authorize the request and store authorization credentials.
def get_service(channel_id):
  credentials = None
  # Check if credentials file exists
  CREDENTIALS_FILE = channel_id + ".json"
  if os.path.exists(CREDENTIALS_FILE):
    credentials = Credentials.from_authorized_user_file(CREDENTIALS_FILE, SCOPES)
  else:
    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
    credentials = flow.run_console()

  # If no valid credentials, use InstalledAppFlow to authenticate
  if not credentials or not credentials.valid:
    if credentials and credentials.expired and credentials.refresh_token:
      print(credentials.refresh_token)
      credentials.refresh(Request())


  # Save the credentials for the next run
  with open(CREDENTIALS_FILE, 'w') as json_file:
    json_file.write(credentials.to_json())

  return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)

def write_to_bq(results: pd.DataFrame, table_id: str):
    credentials, project_id = google.auth.default()

    print("Writing the resulting DataFrame to BigQuery...")
    pandas_gbq.to_gbq(results, table_id, if_exists='append', project_id=project_id, credentials=credentials)

# Call the YouTube Reporting API's reports.list method to get the URL of the latest report of a given job
def retrieve_latest_report_URL(job_id, youtube_reporting, content_owner_id):

  latest_reports = youtube_reporting.jobs().reports().list(
       onBehalfOfContentOwner = content_owner_id,
       jobId = job_id,
       startTimeAtOrAfter=(datetime.datetime.utcnow() - datetime.timedelta(days=3)).isoformat() + "Z" # list only the report for the latest day
  ).execute()

  if 'reports' in latest_reports and latest_reports['reports']:
    report = latest_reports['reports'][0]
    return report['downloadUrl']


# Call the YouTube Reporting API's media.download method to download the report.
def download_report(youtube_reporting, report_url):
    request = youtube_reporting.media().download(
      resourceName=' '
    )
    request.uri = report_url
    
    # Create an in-memory bytes buffer
    buffer = BytesIO()
    
    # Stream/download the report in a single request.
    downloader = MediaIoBaseDownload(buffer, request, chunksize=-1)

    done = False
    while done is False:
        status, done = downloader.next_chunk()
        if status:
            print('Download %d%%.' % int(status.progress() * 100))
    
    print('Download Complete!')
    
    # Go back to the start of the bytes buffer.
    buffer.seek(0)
    
    # Read the buffer content into a Pandas DataFrame
    df = pd.read_csv(buffer)
    
    return df



# retrieve the latest URL for a given report, download it, and store in a target file as a csv
def process_report(job_id, youtube_reporting, content_owner_id, target_table):
  url = retrieve_latest_report_URL(job_id,youtube_reporting,content_owner_id)
  df = download_report(youtube_reporting, url)

  if 'date' in df.columns:  # Check if 'date' column exists
    df['date'] = pd.to_datetime(df['date'].astype(str), format='%Y%m%d')
    
  write_to_bq(df, target_table)
  return (f"processed job {job_id}")



def execute(self):
  with open("job_records.json", "r") as f:
    data_for_report_retrieval = json.load(f)
  
  content_owner_id = data_for_report_retrieval["content_owner_id"]
  youtube_reporting = get_service(content_owner_id)

  job_records = data_for_report_retrieval["job_records"]
  for job_record in job_records:
    job_id = job_record["id"]
    target_table = job_record["targetTable"]

    process_report(job_id, youtube_reporting, content_owner_id, target_table)


  return "ok"
